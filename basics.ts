//Primitives: number, string, boolean
//More complex types: arrays, objescts
//Function types, parameters

//Primitives
let age: number;
age = 22;

let username: string;
username = "Adriana";

let isInstructor: boolean;
isInstructor = true;

//More complex types
let hobbies: string[];
hobbies = ["Dancing", "read"];

let person: {
  name: string;
  age: number;
};
person = {
  name: "Adriana",
  age: 23,
};

let people: {
  name: string;
  age: number;
}[];

//Type inference

let course: string | number = "React- the complete guide";
course = 12334;

//functions and types
function agregar(a: number, b: number) {
    return a + b;
}

function print(value: any) {
    console.log(value);
}

//Generics
function insertAtBeginning<T>(array: T[], value: T) {
    const newArray = [value, ...array];
    return newArray;
}

const demoArray = [1,2,3];
const updateArray = insertAtBeginning(demoArray, -1); //[-1, 1, 2, 3]
const stringArray = insertAtBeginning(['a', 'b', 'c'], 'd')